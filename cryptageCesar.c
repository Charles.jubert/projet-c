#include <stdio.h>
#include <stdlib.h>



void flush()
{
	while( getchar() != '\n' )
	continue;
}
void Crypter(int index, int taille_fichier,int taille_memoire,char * memoire,int decalage,char nom_fichier[256]);
void Dechiffrement(int index, int taille_fichier,int taille_memoire,char * memoire,int decalage,char nom_fichier[256]);
// programme principal

int main()
{
	int decalage = 0;
	char nom_fichier[256];
	char choix;
	int index;
	int taille_fichier = 0;
					
	int taille_memoire = 256;
	char * memoire = NULL;
// choix
	printf( "Code de Cesar\n" );
	printf( "\nQuel décalage voulez vous : " );
	scanf( "%d", &decalage );
	if(decalage == 0){
		printf("\nle texte ne change pas. "); 
	}
	flush();
	printf( "\nSaisir le nom du fichier : " );
	scanf( "%s", nom_fichier );
	flush();
	printf( "\n Faire un Chiffrement [c] ou faire un déchiffrement [d] : " );
	scanf( "%c", &choix );
	if(choix != 'c' || 'd')
	{
	printf("\n Choisir la lettre c ou d obligatoirement!\n");
	}
// codage des deux différent choix c et d a l'aide d'un switch
	switch( choix )
	{
		case 'c' :
		printf("\nNous avons effectuer le cryptage du fichier %s avec le décalage %d.\n", nom_fichier, decalage );
			{
			//crypter
			Crypter( index,  taille_fichier, taille_memoire, memoire, decalage, nom_fichier);
			
				return 0;
				case 'd' :
				printf("\nNous avons effectuer le  déchiffrement du fichier %s avec le décalage  %d.\n", nom_fichier, decalage );
				{
				Dechiffrement( index,  taille_fichier, taille_memoire, memoire, decalage, nom_fichier);
					
				return 0;
				
				
	}
}
}
}
void Crypter(int index, int taille_fichier,int taille_memoire,char * memoire,int decalage,char nom_fichier[256]){
				
				FILE * avant = NULL;
				FILE * apres = NULL;
				
// allocation de la memoire
				memoire = (char*)malloc(taille_memoire);
// mise en mémoire tampon
				avant = fopen( nom_fichier, "r" );
				taille_fichier = fread( memoire, 1, taille_memoire, avant );
				fclose( avant );
// chiffrement dans le mémoire tampon


				for( index = 0; index != taille_fichier; ++index )
					{
// chiffrement des majuscules avec le placement dans la table ascii
						if( memoire[ index ] >=65 && memoire[ index ] < 91 )
						memoire[ index ] = ( ( memoire[ index ] - 65 ) + decalage ) % 26 + 65;
// chiffrement des minuscules avec la placement dans la table ascii
						if( memoire[ index ] >=97 && memoire[ index ] < 123 )
						memoire[ index ] = ( ( memoire[ index ] - 97 ) + decalage ) % 26 + 97;
//erreur caractère spéciaux avec le placement dans la table ascii
						if( memoire[ index ] >=33 && memoire[ index ] <= 47 || memoire[ index ] >= 58 && 							memoire[ index ] <= 64 || memoire[ index ] >= 91 && memoire[ index ] <= 93)
						printf("erreur caractère spécial dans le texte\n");

					}
					
					

// mise a jour du fichier apres execution
					apres = fopen( nom_fichier, "w" );
					fwrite( memoire, 1, taille_fichier, apres );
					fclose( apres );
// desallocation pour une prochaine utilisation sans problème
					free( memoire);
				}
				
				
				
				void Dechiffrement(int index, int taille_fichier,int taille_memoire,char * memoire,int decalage,char nom_fichier[256]){
					FILE * avant = NULL;
					FILE * apres = NULL;
					
// allocation
					memoire = (char*)malloc(taille_memoire);
// mise en mémoire tampon
					avant = fopen( nom_fichier, "r" );
					taille_fichier = fread( memoire, 1, taille_memoire, avant );
					fclose( avant );
// déchiffrement dans le mémoire tampon

					for( index = 0; index != taille_fichier; ++index )
						{
// déchiffrement des majuscules ( meme etape mais nous utilisons - decalage et non + decalage )
							if( memoire[ index ] >=65 && memoire[ index ] < 91 )
							memoire[ index ] = ( ( memoire[ index ] - 65 ) - decalage ) % 26 + 65;
// déchiffrement des minuscules
							if( memoire[ index ] >=97 && memoire[ index ] < 123 )
							memoire[ index ] = ( ( memoire[ index ] - 97 ) - decalage ) % 26 + 97;
						}
		     
// mise a jour du fichier apres utilisation du code
						apres = fopen( nom_fichier, "w" );
						fwrite( memoire, 1, taille_fichier, apres );
						fclose( apres );
// desallocation 
						free( memoire);
				}